# F-Zero GX and F-Zero AX

**Developer**: Amusement Vision

**Release**: 2003

| Game      | Region   | Code     |
| --------- | -------- | -------- |
| F-Zero GX | Japan    | `GFZJ01` |
| F-Zero GX | Americas | `GFZE01` |
| F-Zero GX | Europe   | `GFZP01` |
| F-Zero AX | Japan    | `GFZJ8P` |

*<u>F-Zero GX</u>* (GameCube X) and *<u>F-Zero AX</u>* (Arcade X) are games developed by Amusement Vision, a now-defunct development house of SEGA. Both games are built on the same foundation, differing mostly in  minor ways such as release platform: GX was released on GameCube and AX on the Triforce arcade platform. *<u>F-Zero AX</u>*'s build is earlier than that of *<u>F-Zero GX</u>* and so there are minor differences between files.

