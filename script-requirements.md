# Script Requirements

## Python 3.X

I have not done extensive testing, but I have been working with Python3.8.3. I presume any stable and current-ish Python 3.X version will suffice.

## pyTableWriter

Webpage: https://pypi.org/project/pytablewriter/

This project uses `pytablewriter` to convert CSV files to markdown tables, which are then included using another vanilla script.

Install `pytablewriter` with `pip`:

* `pip install pytablewriter`
* Alternatively, install with all dependencies using `pip install pytablewriter[all]`

## Visual Basic (?)

The current mini toolchain relies on a VBS script to convert XLS/X files to CSV. To my knowledge, this requires the host platform to be Windows. I am using Windows 10.
