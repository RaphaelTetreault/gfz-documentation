import fileinput
from os import getcwd, listdir
from os.path import isfile, join
import re

# the keyword to search for
keyword = "<include:"
keywordRegex = keyword + "(.*?)" + ">"
# file parameters when search directory
prefix = "process."
extension = ".md"


# get current working directory
workingDir = getcwd()
print("Working directory: " + workingDir)

# get all files in this directory
allFiles = [f for f in listdir(workingDir) if isfile(join(workingDir, f))]

# iterate over all files in collection
for file in allFiles:
    # check to see if file is markdown file
    if file.startswith(prefix) and file.endswith(extension):
        print("Processing file: " + file)
        processedFileName = file.replace(prefix, "")
        processedFile = open(processedFileName, 'w')
        print("Created file: " + processedFileName)
        for line in fileinput.input(file):
            # copy file contests if tag matches
            if keyword in line:
                # regex capture between "<include:" and ">"
                #match = re.search(r"include:([A-Za-z0-9_]+)", line)
                match = re.search(keywordRegex, line)
                includeFilePath = match.group(1)
                print("Found include: " + line.replace("\n", ""))
                # go in 1 folder deeper, get md table
                # TODO: was not working. What is proper way to step?
                includeFile = open(includeFilePath)
                # Write contents of file.
                # NOTE: replaces whole line
                print("Writing contents of file: " + includeFilePath)
                for inclideLine in includeFile:
                    processedFile.write(inclideLine)
            else:
                # write line as normal
                processedFile.write(line)
        #close file
        processedFile.close()
        print("")

#input("Press Enter to exit...")
