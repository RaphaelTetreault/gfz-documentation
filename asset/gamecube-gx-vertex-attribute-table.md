## Table of Contents

[TOC]

## Revisions  Table

| Author            | Date       | Version | Description      |
| ----------------- | ---------- | ------- | ---------------- |
| Raphaël Tétreault | 2020/05/07 | 1.0     | Initial document |

# GameCube VAT

The GameCube graphics (GX) pipeline uses customizable definitions for vertex data using Vertex Attribute Formats. The Vertex Attribute Table (VAT) houses up to 8 user-defined configurations at once. While most GameCube games do not use more than 1, F-Zero GX uses 2.

## Vertex Attribute Table

Empty cells mean there is no definition for that VAT that is known as used by F-Zero GX. They may be initialized, but the game's assets do not reference them.

### VAT Count

- XYZ indicates that the position/normal is 3D.
- ST indicates that the UV coordinate space is 2D.
- RGBA indicates that the color components are in RGBA format; 8bits each, 32 bits per color.

### VAT Comp

Comp is short for component. A component in this case is an individual number; ie. the X, Y, and Z component of a position or normal.

- F32 indicates that each position/normal component is stored as 32bit floating point numbers.
- S16 indicates that each position/normal component is stored a 16bit signed fixed-point number. Normals are store as 2.14 thus can represent +1 to -1 values with 14 binary fractional digits. To convert fixed to float, do `float component = (float)compS16 / (1 << nFractions);` where `nFractions` is the number of fractional places.

### VAT

|      | VAT 0 Count | VAT 0 Comp | VAT 1 Count | VAT 1 Comp             |
| ---- | ----------- | ---------- | ----------- | ---------------------- |
| POS  | XYZ         | F32        | XYZ         | S16 (Fixed Point 3.13) |
| NRM  | XYZ         | F32        | XYZ         | S16 (Fixed Point 2.14) |
| NBT  | XYZ         | F32        |             |                        |
| CLR0 | RGBA        | -          | RGBA        | -                      |
| CLR1 |             |            |             |                        |
| TEX0 | ST          | F32        | ST          | S16 (?)                |
| TEX1 | ST          | F32        | ST          | S16 (?)                |
| TEX2 | ST          | F32        | ST          | S16 (?)                |
| TEX3 |             |            |             |                        |
| TEX4 |             |            |             |                        |
| TEX5 |             |            |             |                        |
| TEX6 |             |            |             |                        |
| TEX7 |             |            |             |                        |

NOTE: NBT is has 3 components of Normal, Binormal, and Tangent. in VAT0, this means each NBT entry is 9 floats long, 3 for each component.

## Statistics

These stats were compiled manually using Excel and so may be off. However, the general numbers should be representative enough even if there are errors.

### Overview

#### VAT0

* Has 18867 material definitions.

#### VAT1

* Has 10917 material definitions

#### Misc.

* It looks like GX_TRIANGLESTRIP is used exclusively. Other formats may be used but would be obfuscated at this point. (There are null entries for some primitive formats, perhaps for empties or perhaps for model data.)

### Display List Breakdown

The tables below indicate how many materials reference to each component type in the VAT.

#### Display List 0 - MT

|          | VAT 0 | VAT 1 |
| -------- | ----- | ----- |
| POS      | 18867 | 10917 |
| NRM      | 18679 | 10917 |
| NBT      | 188   | 0     |
| CLR0     | 2639  | 363   |
| CLR1     | 0     | 0     |
| TEX0     | 18811 | 10877 |
| TEX1     | 6532  | 2787  |
| TEX2     | 1178  | 1535  |
| TEX3     | 0     | 0     |
| TEX4     | 0     | 0     |
| TEX5     | 0     | 0     |
| TEX6     | 0     | 0     |
| TEX7     | 0     | 0     |
| PNMTXIDX | 1800  | 2972  |

#### Display List 1 - TL MT

|          | VAT 0 | VAT 1 |
| -------- | ----- | ----- |
| POS      | 20706 | 9078  |
| NRM      | 20518 | 9078  |
| NBT      | 188   | 0     |
| CLR0     | 2694  | 308   |
| CLR1     |       | 0     |
| TEX0     | 20646 | 9042  |
| TEX1     | 7102  | 2217  |
| TEX2     | 1539  | 1174  |
| TEX3     |       | 0     |
| TEX4     |       | 0     |
| TEX5     |       | 0     |
| TEX6     |       | 0     |
| TEX7     |       | 0     |
| PNMTXIDX |       | 3568  |

#### Extra Display Lists

Ex 1 + 2 (double for both combined)

VAT 1 + DisplayListEx is used exclusively for (blood_1000.gma)

|          | VAT 0 | VAT 1 |
| -------- | ----- | ----- |
| POS      | 1     | 7     |
| NRM      | 1     | 7     |
| NBT      |       |       |
| CLR0     |       |       |
| CLR1     |       |       |
| TEX0     | 1     | 7     |
| TEX1     |       | 1     |
| TEX2     |       |       |
| TEX3     |       |       |
| TEX4     |       |       |
| TEX5     |       |       |
| TEX6     |       |       |
| TEX7     |       |       |
| PNMTXIDX | 1     | 7     |

### Reference

This is a copy of the 7 objects in the entire game that use the extra display list data.

| File Name       | Model Name      | Address    | Model Index | Mat Index | Tex Index | Zero 0x00 | Unk_0x02 | Unk_0x03   | Color0                   | Color1                   | Color2                   | Unk_0x10                           | Unk_0x11                                                     | Unk_0x12 | Vertex Render Flags                                          | Unk_0x14 | Unk_0x15 | Tex 0 Index | Tex 1 Index | Tex 2 Index | Vertex Descriptor Flags                                      | DISP0 FMT  | DISP0 PRIM       | DISP1 FMT  | DISP1 PRIM       | DISP EX0 FMT | DISP EX0 PRIM    | DISP EX1 FMT | DISP EX1 PRIM    | Transform Matrix Indexes                        | Mat display list size | Tl mat display list size | Bounding Sphere Origin | Zero 0x3C | Unk_0x40 |
| --------------- | --------------- | ---------- | ----------- | --------- | --------- | --------- | -------- | ---------- | ------------------------ | ------------------------ | ------------------------ | ---------------------------------- | ------------------------------------------------------------ | -------- | ------------------------------------------------------------ | -------- | -------- | ----------- | ----------- | ----------- | ------------------------------------------------------------ | ---------- | ---------------- | ---------- | ---------------- | ------------ | ---------------- | ------------ | ---------------- | ----------------------------------------------- | --------------------- | ------------------------ | ---------------------- | --------- | -------- |
| blood_1000.gma  | BLOOD_c_hara    | 0x00000680 | GCMF[0/2]   | MAT[2/17] | TEX[2/17] | 0         | 0        | 0          | RGBA(178, 178, 178, 255) | RGBA(127, 127, 127, 255) | RGBA(255, 255, 255, 255) | UNK_FLAG_1, UNK_FLAG_3, UNK_FLAG_5 | UNK_FLAG_0, UNK_FLAG_1, UNK_FLAG_2, UNK_FLAG_3, UNK_FLAG_4, UNK_FLAG_5,  UNK_FLAG_6, UNK_FLAG_7 | 1        | RENDER_DISPLAY_LIST_0, RENDER_DISPLAY_LIST_1, RENDER_EX_DISPLAY_LIST_0,  RENDER_EX_DISPLAY_LIST_1 | -1       | 0        | 1           | -1          | -1          | GX_VA_PNMTXIDX, GX_VA_POS, GX_VA_NRM, GX_VA_TEX0             | GX_VTXFMT0 | GX_TRIANGLESTRIP | GX_VTXFMT0 | GX_TRIANGLESTRIP | GX_VTXFMT0   | GX_TRIANGLESTRIP | GX_VTXFMT0   | GX_TRIANGLESTRIP | MTX IDX(255, 255, 255, 255, 255, 255, 255, 255) | 5504                  | 16256                    | (0.0, -0.1, 0.0)       | 0         | 0        |
| ead_500.gma     | EAD_c_hara      | 0x00000E00 | GCMF[0/1]   | MAT[6/15] | TEX[6/15] | 0         | 0        | 0          | RGBA(178, 178, 216, 255) | RGBA(127, 127, 140, 255) | RGBA(127, 127, 140, 255) | UNK_FLAG_3                         | UNK_FLAG_0, UNK_FLAG_1, UNK_FLAG_2, UNK_FLAG_3, UNK_FLAG_4, UNK_FLAG_5,  UNK_FLAG_6, UNK_FLAG_7 | 1        | RENDER_DISPLAY_LIST_0, RENDER_DISPLAY_LIST_1, RENDER_EX_DISPLAY_LIST_0,  RENDER_EX_DISPLAY_LIST_1 | -1       | 0        | 5           | -1          | -1          | GX_VA_PNMTXIDX, GX_VA_POS, GX_VA_NRM, GX_VA_TEX0             | GX_VTXFMT1 | GX_TRIANGLESTRIP | GX_VTXFMT1 | GX_TRIANGLESTRIP | GX_VTXFMT1   | GX_TRIANGLESTRIP | GX_VTXFMT1   | GX_TRIANGLESTRIP | MTX IDX(255, 255, 255, 255, 255, 255, 255, 255) | 1760                  | 4544                     | (0.0, 0.1, 0.0)        | 0         | 0        |
| ead_1000.gma    | EAD_c_hara      | 0x00001840 | GCMF[0/1]   | MAT[7/22] | TEX[7/20] | 0         | 0        | UNK_FLAG_3 | RGBA(255, 255, 255, 255) | RGBA(178, 178, 178, 255) | RGBA(127, 127, 140, 255) | UNK_FLAG_3                         | UNK_FLAG_0, UNK_FLAG_1, UNK_FLAG_2, UNK_FLAG_3, UNK_FLAG_4, UNK_FLAG_5,  UNK_FLAG_6, UNK_FLAG_7 | 1        | RENDER_DISPLAY_LIST_0, RENDER_DISPLAY_LIST_1, RENDER_EX_DISPLAY_LIST_0,  RENDER_EX_DISPLAY_LIST_1 | -1       | 0        | 6           | -1          | -1          | GX_VA_PNMTXIDX, GX_VA_POS, GX_VA_NRM, GX_VA_TEX0             | GX_VTXFMT1 | GX_TRIANGLESTRIP | GX_VTXFMT1 | GX_TRIANGLESTRIP | GX_VTXFMT1   | GX_TRIANGLESTRIP | GX_VTXFMT1   | GX_TRIANGLESTRIP | MTX IDX(255, 255, 255, 255, 255, 255, 255, 255) | 1312                  | 6048                     | (0.0, 0.1, 0.0)        | 0         | 0        |
| falcon_500.gma  | FALCON_c_hara   | 0x000006A0 | GCMF[0/1]   | MAT[3/15] | TEX[3/15] | 0         | 0        | 0          | RGBA(178, 178, 178, 255) | RGBA(127, 127, 127, 255) | RGBA(255, 255, 255, 255) | UNK_FLAG_1, UNK_FLAG_3, UNK_FLAG_5 | UNK_FLAG_0, UNK_FLAG_1, UNK_FLAG_2, UNK_FLAG_3, UNK_FLAG_4, UNK_FLAG_5,  UNK_FLAG_6, UNK_FLAG_7 | 1        | RENDER_DISPLAY_LIST_0, RENDER_DISPLAY_LIST_1, RENDER_EX_DISPLAY_LIST_0,  RENDER_EX_DISPLAY_LIST_1 | -1       | 0        | 2           | -1          | -1          | GX_VA_PNMTXIDX, GX_VA_POS, GX_VA_NRM, GX_VA_TEX0             | GX_VTXFMT1 | GX_TRIANGLESTRIP | GX_VTXFMT1 | GX_TRIANGLESTRIP | GX_VTXFMT1   | GX_TRIANGLESTRIP | GX_VTXFMT1   | GX_TRIANGLESTRIP | MTX IDX(255, 255, 255, 255, 255, 255, 255, 255) | 3072                  | 6592                     | (0.0, -0.1, 0.0)       | 0         | 0        |
| falcon_1000.gma | FALCON_c_hara   | 0x00000600 | GCMF[0/1]   | MAT[2/16] | TEX[2/16] | 0         | 0        | 0          | RGBA(178, 178, 178, 255) | RGBA(255, 255, 255, 255) | RGBA(255, 255, 255, 255) | UNK_FLAG_1, UNK_FLAG_3, UNK_FLAG_5 | UNK_FLAG_0, UNK_FLAG_1, UNK_FLAG_2, UNK_FLAG_3, UNK_FLAG_4, UNK_FLAG_5,  UNK_FLAG_6, UNK_FLAG_7 | 1        | RENDER_DISPLAY_LIST_0, RENDER_DISPLAY_LIST_1, RENDER_EX_DISPLAY_LIST_0,  RENDER_EX_DISPLAY_LIST_1 | -1       | 0        | 1           | -1          | -1          | GX_VA_PNMTXIDX, GX_VA_POS, GX_VA_NRM, GX_VA_TEX0             | GX_VTXFMT1 | GX_TRIANGLESTRIP | GX_VTXFMT1 | GX_TRIANGLESTRIP | GX_VTXFMT1   | GX_TRIANGLESTRIP | GX_VTXFMT1   | GX_TRIANGLESTRIP | MTX IDX(255, 255, 255, 255, 255, 255, 255, 255) | 2880                  | 8640                     | (0.0, -0.1, 0.0)       | 0         | 0        |
| octman_3000.gma | OCTMAN_BODY_SKL | 0x00000940 | GCMF[0/4]   | MAT[1/11] | TEX[1/26] | 0         | 0        | UNK_FLAG_3 | RGBA(255, 255, 255, 255) | RGBA(178, 178, 178, 255) | RGBA(0, 0, 0, 0)         | 0                                  | UNK_FLAG_0, UNK_FLAG_1, UNK_FLAG_2, UNK_FLAG_3, UNK_FLAG_4, UNK_FLAG_5,  UNK_FLAG_6, UNK_FLAG_7 | 2        | RENDER_DISPLAY_LIST_0, RENDER_DISPLAY_LIST_1, RENDER_EX_DISPLAY_LIST_0,  RENDER_EX_DISPLAY_LIST_1 | -1       | 0        | 0           | 1           | -1          | GX_VA_PNMTXIDX, GX_VA_POS, GX_VA_NRM, GX_VA_TEX0, GX_VA_TEX1 | GX_VTXFMT1 | GX_TRIANGLESTRIP | GX_VTXFMT1 | GX_TRIANGLESTRIP | GX_VTXFMT1   | GX_TRIANGLESTRIP | GX_VTXFMT1   | GX_TRIANGLESTRIP | MTX IDX(255, 255, 255, 255, 255, 255, 255, 255) | 4032                  | 10240                    | (0.0, -0.4, 0.0)       | 0         | 0        |
| ead_500.gma     | EAD_c_hara      | 0x00000E00 | GCMF[0/1]   | MAT[6/15] | TEX[6/15] | 0         | 0        | 0          | RGBA(178, 178, 216, 255) | RGBA(127, 127, 140, 255) | RGBA(127, 127, 140, 255) | UNK_FLAG_3                         | UNK_FLAG_0, UNK_FLAG_1, UNK_FLAG_2, UNK_FLAG_3, UNK_FLAG_4, UNK_FLAG_5,  UNK_FLAG_6, UNK_FLAG_7 | 1        | RENDER_DISPLAY_LIST_0, RENDER_DISPLAY_LIST_1, RENDER_EX_DISPLAY_LIST_0,  RENDER_EX_DISPLAY_LIST_1 | -1       | 0        | 5           | -1          | -1          | GX_VA_PNMTXIDX, GX_VA_POS, GX_VA_NRM, GX_VA_TEX0             | GX_VTXFMT1 | GX_TRIANGLESTRIP | GX_VTXFMT1 | GX_TRIANGLESTRIP | GX_VTXFMT1   | GX_TRIANGLESTRIP | GX_VTXFMT1   | GX_TRIANGLESTRIP | MTX IDX(255, 255, 255, 255, 255, 255, 255, 255) | 1760                  | 4544                     | (0.0, 0.1, 0.0)        | 0         | 0        |
| falcon_500.gma  | FALCON_c_hara   | 0x000006A0 | GCMF[0/1]   | MAT[3/15] | TEX[3/15] | 0         | 0        | 0          | RGBA(178, 178, 178, 255) | RGBA(127, 127, 127, 255) | RGBA(255, 255, 255, 255) | UNK_FLAG_1, UNK_FLAG_3, UNK_FLAG_5 | UNK_FLAG_0, UNK_FLAG_1, UNK_FLAG_2, UNK_FLAG_3, UNK_FLAG_4, UNK_FLAG_5,  UNK_FLAG_6, UNK_FLAG_7 | 1        | RENDER_DISPLAY_LIST_0, RENDER_DISPLAY_LIST_1, RENDER_EX_DISPLAY_LIST_0,  RENDER_EX_DISPLAY_LIST_1 | -1       | 0        | 2           | -1          | -1          | GX_VA_PNMTXIDX, GX_VA_POS, GX_VA_NRM, GX_VA_TEX0             | GX_VTXFMT1 | GX_TRIANGLESTRIP | GX_VTXFMT1 | GX_TRIANGLESTRIP | GX_VTXFMT1   | GX_TRIANGLESTRIP | GX_VTXFMT1   | GX_TRIANGLESTRIP | MTX IDX(255, 255, 255, 255, 255, 255, 255, 255) | 3072                  | 6592                     | (0.0, -0.1, 0.0)       | 0         | 0        |