# AX Staff Ghost

The table below outlines all of the staff ghosts in <u><i>F-Zero AX</i></u>. Of particular interest is that some ghosts are allotted for courses which are not on disc but indicate which unused. 

StarkNebula's excerpt from TCRF:

> There exists 19 Staff Ghosts despite only 6 being used. They appear to exist for testing purposes and it is unclear if they used *AX's* control scheme and drifting mechanics or *GX's*. The name tagged in each file is either TOKKY for unused data or 開発者 (which means developer) for those that are used, encoded in Shift-JIS. The file size is also dependent on the factor. *F-Zero GX's* ghost data is 16KB, as opposed to the 12KB and 8KB formats seen here. The Cylinder Wave ghost is the only ghost which *AX* and *GX* share, and thus the only 12KB ghost data used in *GX*. *AX's* Thunder Road ghost clocks in 3.901 seconds faster at 3'14"371 versus *GX's* 3'18"272, being the only official ghost faster in *AX* as compared to *GX*.

[Read the full article](https://tcrf.net/F-Zero_AX#Staff_Ghost_Data).



| Ghost Index[1] | Stage Index[2] | Track         | Machine         | Time     | Name[3] | File Size[4] |
| -------------- | -------------- | ------------- | --------------- | -------- | ------- | ------------ |
| 1              | 1              | Twist Road    | Golden Fox      | 1'05"200 | TOKKY   | 8KB          |
| 2              | 14             | Drift Highway | Blue Falcon     | 1'15"066 | TOKKY   | 8KB          |
| 7              | 7              | Aero Dive     | Blue Falcon     | 2'40"811 | TOKKY   | 8KB          |
| 10             | 10             | Intersection  | Blue Falcon     | 2'44"220 | TOKKY   | 8KB          |
| 13             | 13             | Long Pipe     | Blue Falcon     | 2'58"959 | TOKKY   | 8KB          |
| 31             | 31             | Screw Drive   | Rolling Turtle  | 2'06"823 | 開発者  | 12KB         |
| 32             | 32             | Meteor Stream | Fire Stingray   | 2'10"006 | 開発者  | 12KB         |
| 33             | 33             | Cylinder Wave | Fat Shark       | 2'06"837 | 開発者  | 12KB         |
| 34             | 34             | Thunder Road  | Fire Stingray   | 3'14"371 | 開発者  | 12KB         |
| 35             | 35             | Spiral        | Rainbow Phoenix | 3'50"959 | 開発者  | 12KB         |
| 36             | 36             | Sonic Oval    | Fat Shark       | 1'54"721 | 開発者  | 12KB         |
| 37             | 14             | Drift Highway | Blue Falcon     | 1'13"787 | TOKKY   | 8KB          |
| 38             | 8              | Loop Cross    | Blue Falcon     | 2'41"626 | TOKKY   | 8KB          |
| 62             | 9              | Half-Pipe     | Blue Falcon     | 3'34"143 | TOKKY   | 8KB          |
| 64             | 11             | Mobius Ring   | Blue Falcon     | 2'01"517 | TOKKY   | 8KB          |
| 66             | 8              | Loop Cross    | Blue Falcon     | 2'18"536 | TOKKY   | 8KB          |
| 72             | 26             | Surface Slide | Blue Falcon     | 2'16"397 | TOKKY   | 8KB          |
| 76             | 3              | Serial Gaps   | Blue Falcon     | 1'46"929 | TOKKY   | 8KB          |
| 84             | 36             | Sonic Oval    | Wild Goose      | 1'21"497 | TOKKY   | 8KB          |

[1] Ghost Index: the staff ghost data's number index (eg: ghost<u>84</u>.dat).

[2] Stage Index: the correct index to used for any given ghost data for it to load on the appropriate track given the data stage order in the ROM. (eg: ghost84.dat cannot load any stage 84 as it does not exist, but the course layout is that for stage 36).

[3] 開発者 translates to Developer.

[4] Note the two "standards" of 8KB / 12KB files.