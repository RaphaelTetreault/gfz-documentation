# Unknowns List

Last updated: 2021/06/13



1. AnimationCurvePlus
   * 4 unknowns before animation curve. No data on it (or analysis?)
2. ArcadeCheckpointType
   * 2 values. PTCW has unique value. I'm wondering if it is a circle collider instead of a box collider?
3. ColiScene
   * Bool32 use/purpose is unknown
   * Range unkRange is... unknown. From prior tests, didn't do much.
   * Scene object counts 1 and 2 - unknown purpose. What is it counting?
4. ColliderGeo
   * Has 5 unused fields or so. No data on it (or analysis?)
5. Collider Tri/Quad
   * Unknown initial value
   * Unknown precompute values. How are they derived?
6. CourseMetadataTrigger
   * What do the BBO triggers do?
   * What is the proper scale of the capsule triggers?
7. Fog / FogCurves
   * What does the final value [6] do? Fog has "3" potential values, though I suspect some kind of padding. The curve exists for [6], but is always 0. Maybe alpha? (or 1 - value for alpha?).
8. InterpolationMode (Animation keyframe interpolation)
   * Unsure what enums 2 and 3 are exactly.
9. ObjectActiveOverride
   * requires further investigation as to whether or not it really is an "override" or whatnot
10. SceneInstanceReference
    * Unknown enum `UnkInstanceFlag`
    * Unknown enum `UnkInstanceOption`
11. SceneObjectReference
    * Unknown float value. LOD?
12. SceneObject
    * Solve the UnknownObjectBitfield fields
13. SkeletalAnimator & SkeletalProperties
    * All of it, really.
14. StaticColliderMeshProperty
    * Understand what all the death types do exactly
    * Figure out which indexes match between AX and GX
15. StoryObjectTrigger
    * Is it really a trigger?
    * solve pos/rot/scale use
16. TrackCornerTopology
    * Solve condition(s) for when it shows up in track data.
17. TrackTopologyMetadata
    * Get better names for what they really do
18. TrackTransform
    * Solve matrices compounding
    * Solve the other random bits / flags (1, 2, 4?)
    * Solve UnknownTrackOption2
19. TriggerableAnimation
    * Solve how the flag indices correlate to animations firing
20. TriggerableVisualEffect
    * Compile information from GW9
21. UnknownSceneObjectData
    * Solve purpose of UnknownSceneObjectData data [12]. Indexes used: 0-3.
      * Leads to understanding UnknownSceneObjectFloatPair
22. UnknownSolsTrigger
    * Is it a trigger? (data would suggest). How big?
    * What does it do?
23. UnknownTransformOption
    * Not worth caring about, but what is it? Hardly used.
24. UnknownTrigger
    * Used in most scenes, used A LOT.
    * What does it doe? There are two 16-bit fields.