# Asset Formats - README

(TODO: explain purpose of folder)

In this documentation, you will find references to mostly built-in data types. The table below describes the types below.

## Primitive Types

Primitive types are the base types provided by most every programming language. Tables in this documentation uses the notation to describe the bits/bytes of each type and the data it stores.

| Notation | Bits | Bytes | Description                            |
| -------- | ---- | ----- | -------------------------------------- |
| bool     | 8    | 1     | boolean: true or false                 |
| double   | 64   | 8     | double-precision floating point number |
| float    | 32   | 4     | single-precision floating point number |
| int8     | 8    | 1     | signed integer                         |
| int16    | 16   | 2     | signed integer                         |
| int32    | 32   | 4     | signed integer                         |
| int64    | 64   | 8     | signed integer                         |
| uint8    | 8    | 1     | unsigned integer                       |
| uint16   | 16   | 2     | unsigned integer                       |
| uint32   | 32   | 4     | unsigned integer                       |
| uint64   | 64   | 8     | unsigned integer                       |
| float    | 32   | 4     | floating point number                  |

## Math/Vector Types

The types described below are use for vector math such as describing position, rotation, and scale, UV/texture coordinates, and more.

| Notation   | Bits | Bytes | Description                                              |
| ---------- | ---- | ----- | -------------------------------------------------------- |
| float2     | 64   | 8     | 2 floating point numbers, represents x/y coordinates     |
| float3     | 96   | 12    | 3 floating point numbers, represents x/y/z coordinates   |
| float4     | 128  | 16    | 4 floating point numbers, represents x/y/z/w coordinates |
| quaternion | 128  | 16    | 4-dimensional rotation                                   |

## Other Types

The types described below are reoccurring concepts that are simplified using specific interpretations.

| Notation     | Bits         | Bytes        | Description                                                  |
| ------------ | ------------ | ------------ | ------------------------------------------------------------ |
| Pointer      | 32           | 4            | 32bit pointer. Stores address.                               |
| ArrayPointer | 32           | 8            | 32bit array pointer. Stores length, then address.            |
| CString      | 8 (per char) | 1 (per char) | A C string. Characters are stored as 8bit values. The string is null-terminated. |

## Arrays

For sake of brevity, arrays are denoted using square brackets.

### Fixed-Length Arrays

If the array is of variable length, the marker `[]` is used.

### Variable-Length Arrays

If the array is of fixed length, the marker `[#]` is used, where `#` is the constant size of the array. For example, `uint32[8]` denotes an array of `uint32` of fixed length 8. 