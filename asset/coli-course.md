# COLI_COURSE

This document will cover only the COLI_COURSE format used in <u>*F-Zero GX*</u> (GFZE01, GFZJ01, GFZP01) and *<u>F-Zero AX</u>* (GFZJ8P).



## Revisions  Table

| Author            | Date       | Version | Description      |
| ----------------- | ---------- | ------- | ---------------- |
| Raphaël Tétreault | 2019/05/11 | 1.0     | Initial document |
| Raphaël Tétreault | 2020/05/07 | 1.1     | Add GameObject   |



## Table Of Contents

[TOC]

# Introduction

The COLI format is almost better described by what it *doesn't* store rather than what it does. In terms of a playable course, the COLI format stores everything *except* raw model data and texture data. In more modern terms, it is a scene file with object placement, transforms, animations, and more, including the elusive track data (spline, collision meshes, AI information). 

## About this Deconstruction

There are many ways of interpreting the raw binary into something useable. The approach used in this deconstruction keeps in mind 2 constraints:

1. Object-Oriented Programming
2. Unity Engine integration

The data representation here generally follows the concept that each object houses both state and function and that the data should be viewable and (ideally) editable within the Unity Editor. As such, data is hierarchical and often times grouped into logical entities even if the structure could be streamlined further.

# Structure

This data is a lot of things at once. The name would make you think it's course collision data.  It is, but it's *much* more than that.

## Header

The data begins with a header. The header is 216 (0xD8) bytes long in F-Zero GX and 212 (0xD4) bytes long in F-Zero AX. Note the offsets on the left for each game.

The header's purpose is to store references to most everything else in the scene file. It also stores some local data relating to the track.

| Offset GX | Offset AX | Type        | Var Name               | Description                                                  |
| --------- | --------- | ----------- | ---------------------- | ------------------------------------------------------------ |
| 0x00      | 0x00      | float       | Unknown 0x00           |                                                              |
| 0x04      | 0x04      | float       | Unknown 0x04           |                                                              |
| 0x08      | 0x08      | int32       | Track Spline Count     | Count of 12 byte structure                                   |
| 0x0C      | 0x0C      | int32       | Track Spline AbsPtr    |                                                              |
| 0x10      | 0x10      | int32       | 0x14 Count             | Count of 20 byte structure                                   |
| 0x14      | 0x14      | int32       | AI Collision Placement | Collision placement in format usable by AI - size, type, track branch ID - Heal, Boost, Jump pad |
| 0x18      | 0x18      | bool32      | Is Boost Pads Enabled  | Enables boost pads when TRUE                                 |
| 0x1C      | 0x1C      | int32       | Effects Collision Mesh | The mesh data for all collision types. See [Track Collision Properties Table](#track-collision-properties-table). |
| 0x20      | 0x20      | int32       |                        |                                                              |
| 0x24      | 0x24      | int32       |                        |                                                              |
| 0x28      | 0x28      | int8 [0x20] |                        |                                                              |
| 0x48      |           | int32       |                        | (!) Not part of AX structures.                               |
| 0x4C      | 0x48      | int32       | Game Object Count      | Number of elements in Game Object array. Count of 64 bytes per structure. |
| 0x50      | 0x4C      | int32       | Game Object AbsPtr     | Absolute pointer to Game Object array. See [GameObject](#GameObject). |
| 0x54      | 0x50      | int32       |                        |                                                              |
| 0x58      | 0x54      | bool32      |                        |                                                              |
| 0x5C      | 0x58      | 32          | Zero 0x5C              |                                                              |
| 0x60      | 0x5C      | int32       |                        |                                                              |
| 0x64      | 0x60      | int32       |                        |                                                              |
| 0x68      | 0x64      | int32       |                        |                                                              |
| 0x6C      | 0x68      | int32       |                        |                                                              |
| 0x70      | 0x6C      | int32       |                        |                                                              |
| 0x74      | 0x70      | int32       |                        |                                                              |
| 0x78      | 0x74      | 32          | Zero                   |                                                              |
| 0x7C      | 0x78      | 32          | Zero                   |                                                              |
| 0x80      | 0x7C      | 16/16       |                        |                                                              |
| 0x84      | 0x80      | int32       |                        |                                                              |
| 0x88      | 0x84      | int32       |                        |                                                              |
| 0x8C      | 0x88      |             | Zero                   | Constant 0                                                   |
| 0x90      | 0x8C      |             | Zero                   |                                                              |
| 0x94      | 0x90      | int32       |                        |                                                              |
| 0x98      | 0x94      | int32       |                        |                                                              |
| 0x9C      | 0x98      | int32       |                        |                                                              |
| 0xA0      | 0x9C      | int32       |                        |                                                              |
| 0xA4      | 0xA0      | int32       |                        |                                                              |
| 0xA8      | 0xA4      | int32       |                        |                                                              |
| 0xAC      | 0xA8      | int32       |                        |                                                              |
| 0xB0      | 0xAC      | int32       |                        |                                                              |
| 0xB4      | 0xB0      | int32       |                        |                                                              |
| 0xB8      | 0xB4      | int32       |                        |                                                              |
| 0xBC      | 0xB8      | int32       |                        |                                                              |
| 0xC0      | 0xBC      | float       |                        |                                                              |
| 0xC4      | 0xC0      | float       |                        |                                                              |
| 0xC8      | 0xC4      | float       |                        |                                                              |
| 0xCC      | 0xC8      | float       |                        |                                                              |
| 0xD0      | 0xCC      |             | 00.00.00.08            |                                                              |
| 0xD4      | 0xD0      |             | 00.00.00.08            |                                                              |
| 0xD8      | 0xD4      | int8 [0x24] |                        |                                                              |



### Track Collision Properties Table

Some early tracks available in the *<u>F-Zero AX</u>* ROM have naming conventions with a display index correlating to the type of track property it has which is then serialized as triangles in the COLI_COURSE file. 

| Display Index | Confirmed (Zero Index) | Description                                          |
| ------------- | ---------------------- | ---------------------------------------------------- |
| 1             |                        |                                                      |
| 2             | 1                      | Recover                                              |
| 3             |                        |                                                      |
| 4             |                        | "Hitbox/Wall" Hard Surface                           |
| 5             | 3                      | Boost Plate                                          |
| 6             | 4                      | Jump Pad                                             |
| 7             |                        | Ice/Slip                                             |
| 8             |                        | Dirt                                                 |
| 9             |                        | Mine                                                 |
| 10            |                        | Best guess: Invert Gravity (invG in early AX stages) |
| 11            |                        |                                                      |
| 12            |                        |                                                      |
| 13            |                        |                                                      |
| 14            |                        |                                                      |
| 15            |                        |                                                      |

| Missing Unconfirmed Indices                                  |
| ------------------------------------------------------------ |
| Lava / Track underside                                       |
| Drivable surface (used for track and ramp in CH5)            |
| AX Checkpoint? (could be a trigger volume, a track point as float + distance vector) |



## TrackTransform

* THIS REALLY NEEDS RE-EVALUATION

| Offset | Type                            | Var Name         | Description                                                  |
| ------ | ------------------------------- | ---------------- | ------------------------------------------------------------ |
| 0x00   | Track Transform Hierarchy Depth | Unknown          | Flags (?): 0, 1, 2, 3                                        |
| 0x01   |                                 | Unknown          | Flags (?): 1,2,3,4,5,6,7                                     |
| 0x02   |                                 | Unknown          | Flags: 4, 8, 12, 20, 28, 44                                  |
| 0x03   |                                 | Unknown          | 0, 1, 2                                                      |
| 0x04   | int32                           | Topology Abs Ptr | Points to a number of arrays. See [Topology Parameters](#topology-parameters) |
| 0x08   | int32                           | Unknown Abs Ptr  | Points to a Transform-like structure (0x34 bytes)            |
| 0x0C   | int32                           | Child Count      | Child count (MAX DEPTH 6)                                    |
| 0x10   | int32                           | Children Abs Ptr |                                                              |
| 0x14   | float3                          | Unknown          | Looks like Scale most of the time...                         |
| 0x20   | float3                          | Unknown          | Looks like Rotation most of the time...                      |
| 0x2C   | float3                          | Unknown          | Looks like Position most of the time...                      |
| 0x38   |                                 | Unknown          |                                                              |
| 0x3C   |                                 | Unknown          | 0, 2, 3, 4, 5, 6, 7, 8, 10, 12, 20, 23                       |
| 0x40   |                                 | Unknown          | 0, 2, 3, 4, 5, 6, 7, 8, 10, 12, 20                           |
| 0x44   | int32                           | Zero 0x44        | Constant 0                                                   |
| 0x48   | int32                           | Zero 0x48        | Constant 0                                                   |
| 0x4C   |                                 | Unknown          | 0, 1, 2, 3                                                   |



## Topology Transform Parameters

There exist separate parameters which modify the state of the track spline at different intervals. The structure, 0x14 bytes long, consists of a flag value, a (non-normalized) LERP (linear-interpolation) value, an X, Y, or Z component, and presumably a Left and Right value modifier.

These appear in sets of arrays, typically in sets of 2 or 21. This needs to be investigated further as it has serious implications on managing the data types.

The structure is essentially a set of counts and pointers. It has a strange serialized structure as it stores 9 counts then 9 pointers (ie: not interleaved as one might expect). Perhaps a 2-dimensional array.

| Offset | Type      | Name         | Description                                   |
| :----- | --------- | ------------ | --------------------------------------------- |
| 0x00   | int32 [9] | Counts       | Count for topology transform modifier         |
| 0x24   | int32 [9] | Abs Pointers | Absolute pointer to topology transform arrays |

Each count/pointer pair is order as follows

1. Scale X
2. Scale Y
3. Scale Z
4. Rotation X
5. Rotation Y
6. Rotation Z
7. Position X
8. Position Y
9. Position Z

## Topology Transform Parameter

* hey this is probably just KeyableAttribute from the anim systems since this is probably data derived from spline model data. I copied the data over for now

| Offset | Type    | Name        | Description                                                  |
| ------ | ------- | ----------- | ------------------------------------------------------------ |
| 0x00   | Flags32 | Ease Mode   | { 0, 1, 2, 3 } my intuition would say these are either track types or interpolation functions (eg: LERP, SLERP, QUAD, SQUAD...) |
| 0x04   | float   | Time        | The interpolation "time". Typically 0-1000, but includes negative values too. |
| 0x08   | float   | Value       | The float value to interpolate                               |
| 0x0C   | float   | zTangentIn  | The tangent angle (z) into the node.                         |
| 0x10   | float   | zTangentOut | The tangent angle (z) out the node.                          |

### Statistics

This table lists some statistics for these structures. Please **note** that this is an incomplete view of the data as it does not detail any *relationship* between the stats and which level within hierarchy the values belong to. In other words, better analysis can be performed on this data.

| #    | Freq. | Flags   | Lerp Min | Lerp Max | Comp Min | Comp Max | Comp Avg | L/R Min  | L/R Max  |
| ---- | ----- | ------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- |
| 1    | 5981  | 0,1,2,3 | -1.5     | 1000     | 0.00001  | 800.0001 | 301.1171 | -93.6499 | 80.40142 |
| 2    | 982   | 1,2,3   | 0        | 1000     | -5       | 300      | 410.8123 | -0.86    | 0.86     |
| 3    | 416   | 1,2     | 0        | 1000     | 0.001808 | 200      | 355.7268 | -0.3     | 0.6      |
| 4    | 7314  | 1,2     | 0        | 1000     | -360     | 539.9999 | 221.1821 | -455.024 | 561.1468 |
| 5    | 10252 | 1,2     | 0        | 1000     | -805.394 | 720      | 210.1711 | -5370.12 | 5397.969 |
| 6    | 4811  | 1,2     | 0        | 1000     | -990.103 | 900      | 253.7303 | -229.046 | 534.0182 |
| 7    | 8415  | 0,1,2   | 0        | 1000     | -5027.43 | 8200     | 180.3983 | -1089.06 | 1075.318 |
| 8    | 7304  | 1,2,3   | -1.5     | 1000     | -3050.03 | 1500     | 215.1232 | -315.48  | 425.994  |
| 9    | 9184  | 1,2     | 0        | 1000     | -10350   | 3963.145 | 216.4367 | -1096.65 | 1094.121 |

#### [1] Scale X

#### [2] Scale Y

**Note:** only used for:

* [Stage 21] Aeropolis Dragon Slope
* [Stage 35] Green Plant Spiral

#### [3] Scale Z

**Note:** only used for:

- [Stage 21] Aeropolis Dragon Slope
- [Stage 35] Green Plant Spiral
- -0.3  0  0.012886  0.299285  0.3  0.6

#### [4] Rotation X

Relevant to 19 GX, 31 AX stages. 

Some early AX stages appear to use it as rotation.

#### [5] Rotation Y

The most used field of any, with 10,252 occurrences in all AX and GX files combined.

#### [6] Rotation Z

16 GX, 24 AX stages.

#### [7] Position 7

#### [8] Position 8

#### [9] Position Z



## GameObject

An F-Zero `GameObject` is very similar to the Unity concept of GameObjects. Every GameObject is guaranteed to have a Transform component and thus is part of the scene. They may contain peripheral data related to collision, animation, and a few yet-understood functions.

**NOTE**: this structure has not yet been tested with F-Zero AX.

| Offset | Type   | Var Name                                        | Description                                                  |
| ------ | ------ | ----------------------------------------------- | ------------------------------------------------------------ |
| 0x00   |        | Unknown                                         | Appears to be enum flags.                                    |
| 0x04   |        | Unknown                                         | Appears to be enum flags.                                    |
| 0x08   | int32  | [Collision Binding](#collision-binding) Abs Ptr | An absolute pointer to the collision binding data. The binding data references the GMA object by name and the collision data polygons. Always present (TODO: confirm). |
| 0x0C   | float3 | Position                                        | The (collision data?) position/origin.                       |
| 0x18   |        | Unknown                                         | Appears to be enum flags. 16bit                              |
| 0x1A   |        | Unknown                                         | Appears to be enum flags. 16bit                              |
| 0x1C   |        | Unknown                                         | Appears to be enum flags. 16bit                              |
| 0x1E   |        | Unknown                                         | Appears to be enum flags. 16bit                              |
| 0x20   | float3 | Scale                                           | The scale of the (collision?) object.                        |
| 0x2C   | int32  | Zero 0x2C                                       | 2020/05/12: confirmed always 0                               |
| 0x38   | int32  | Animation Abs Ptr                               | An absolute pointer to animation data. Pointer can be null (0). |
| 0x3C   | int32  | Unknown Abs Ptr                                 | An absolute pointer to unknown data. Pointer can be null (0). |
| 0x40   | int32  | Unknown Abs Ptr                                 | An absolute pointer to unknown data related to objects on in the Sand Ocean venue. (Note: dated investigation, confirm.) As implied, pointer can be null (0). |
| 0x44   | int32  | Transform Abs Ptr                               | An absolute pointer to the GameObject's transform. Always present. |



### Collision Binding

Name could be improved.

Data structure points to a linking structure with data for the object's name and also the collision data which may be attached to an object.

| Offset | Type  | Var Name                                        | Description                                                  |
| ------ | ----- | ----------------------------------------------- | ------------------------------------------------------------ |
| 0x00   |       | Unknown                                         |                                                              |
| 0x04   |       | Unknown                                         |                                                              |
| 0x08   | int32 | [Reference Binding](#reference-binding) Abs Ptr | Pointer always exists, points to ReferenceBinding structure. |
| 0x0C   | int32 | Collision Abs Ptr                               |                                                              |



#### Reference Binding

A structure which points into the name table to identify this GameObject, presumably to load the appropriate model via string search.

| Offset | Type  | Var Name     | Description                                                |
| ------ | ----- | ------------ | ---------------------------------------------------------- |
| 0x00   |       | Unknown      | Old notes: null.                                           |
| 0x04   | int32 | Name Abs Ptr | An absolute pointer to the name (C string) of this object. |
| 0x08   |       | Unknown      | Old notes: null.                                           |
| 0x0C   |       | Unknown      | Old notes: may be a float?                                 |



#### Collision

The collision data. 

| Offset | Type  | Var Name         | Description                             |
| ------ | ----- | ---------------- | --------------------------------------- |
| 0x00   |       | Unknown          |                                         |
| 0x04   |       | Unknown          |                                         |
| 0x08   |       | Unknown          |                                         |
| 0x0C   |       | Unknown          |                                         |
| 0x10   |       | Unknown          |                                         |
| 0x14   | int32 | Triangle Count   | Number of triangles for this collision. |
| 0x18   | int32 | Quad Count       | Number of quads for this collision.     |
| 0x1C   | int32 | Triangle Abs Ptr | Pointer to triangle data array.         |
| 0x20   | int32 | Quad Abs Ptr     | Pointer to quad data array.             |



##### Collision Triangle

| Offset | Type   | Var Name     | Description                                                  |
| ------ | ------ | ------------ | ------------------------------------------------------------ |
| 0x00   |        | Unknown      |                                                              |
| 0x04   | float3 | Normal       | Triangle normal                                              |
| 0x08   | float3 | Vertex 0     | Triangle vertex #0                                           |
| 0x0C   | float3 | Vertex 1     | Triangle vertex #1                                           |
| 0x10   | float3 | Vertex 2     | Triangle vertex #2                                           |
| 0x14   | float3 | Precomputed0 | Appears to be a precomputed value for optimized collision detection. Likely the precompute of Vertex 0. |
| 0x18   | float3 | Precomputed1 | Appears to be a precomputed value for optimized collision detection. Likely the precompute of Vertex 1. |
| 0x1C   | float3 | Precomputed2 | Appears to be a precomputed value for optimized collision detection. Likely the precompute of Vertex 2. |



##### Collision Quad

| Offset | Type   | Var Name     | Description                                                  |
| ------ | ------ | ------------ | ------------------------------------------------------------ |
| 0x00   |        | Unknown      |                                                              |
| 0x04   | float3 | Normal       | Quad normal                                                  |
| 0x08   | float3 | Vertex 0     | Quad vertex #0                                               |
| 0x0C   | float3 | Vertex 1     | Quad vertex #1                                               |
| 0x10   | float3 | Vertex 2     | Quad vertex #2                                               |
| 0x14   | float3 | Vertex3      | Quad vertex #3                                               |
| 0x18   | float3 | Precomputed0 | Appears to be a precomputed value for optimized collision detection. Likely the precompute of Vertex 0. |
| 0x1C   | float3 | Precomputed1 | Appears to be a precomputed value for optimized collision detection. Likely the precompute of Vertex 1. |
| 0x20   | float3 | Precomputed2 | Appears to be a precomputed value for optimized collision detection. Likely the precompute of Vertex 2. |
| 0x24   | float3 | Precomputed3 | Appears to be a precomputed value for optimized collision detection. Likely the precompute of Vertex 3. |



##### Notes on Collision Tri/Quad

The unknown float3s at the end of the structure appears to be some precomputed delta-type value. Below is a paste from a conversation I had about this data. Note the COLI Analyzer object has output for the TRI and QUAD data.

> StarkNebulaToday at 11:32 AM
> Hey. Quasi SMB related question. I couldn't readily find data structures defining collision in the WS2 git (along with some of the other projects I had a peek at). I'm currently working on an editor for F-Zero GX/AX and it's (non-track) collision is stored as either tris or quads (standard stuff) but are bundled with some precomputed values. Anyway, sparing some details, is there some documentation on SMB's collision that someone could direct me to (or explain)? I can elaborate if needed.
>
> ComplexPlaneToday at 11:35 AM
> here's SMB2's stagedef format if it's helpful? https://craftedcart.github.io/SMBLevelWorkshop/documentation/index.html?page=stagedefFormat2#spec-stagedefFormat2-section-levelModel
> Documentation - SMB Level Workshop
> SMB Level Workshop - Documentation
>
> StarkNebulaToday at 11:39 AM
> @ComplexPlane Thanks! I'll have a look. They look a little different but the note (Delta X position for the 2nd triangle point from the 1st point, before rotation is applied) sounds similar to what I'm seeing, so will verify.
>
> ![](..\res\image\coli\gameobject\gfz quad.PNG)
>
> StarkNebulaToday at 11:59 AM
> I had a look and it appears like the games do things differently. I'm sure it'll be sorted out at some point. The delta on X and Y components seems to be something that is still going on. However, it looks like the values are precomputed in some way to be powers of 2 (or multiplied multiple times, in cases as highlighted the delta value is repeated).
>
> ![](..\res\image\coli\gameobject\gfx quad delta.PNG)
>
> [12:02 PM]
> The unknown values (32771140.0, 403084900) are divisible by the delta (1705.2, 138.6) in some ways. 32771140 / 1705.1 / 138.6 = 138.6, 403084900 / 1705.1 / 1705.1 = 138.6.(edited)
>
> [12:03 PM]
> Anyways, just getting this out of my system. Thanks for the reference doc, I'll have it handy. If I find something in GX that appears to work the same in SMB I'll let you all know.



### Animation

Another familiar concept for Unity users. The Animation structure outlines a variety of parameters for how to animate and a set of key frames for animating.

| Offset | Type                   | Var Name              | Description                                                  |
| ------ | ---------------------- | --------------------- | ------------------------------------------------------------ |
| 0x00   | float                  | Unknown               |                                                              |
| 0x04   | float                  | Unknown               |                                                              |
| 0x08   | byte[]                 | Zeroes                | A padded area of length 0x10                                 |
| 0x18   | Enum                   | Unknown               | Unknown parameter. Unique values: 1, 2, 3.                   |
| 0x1C   | Animation Key Ptr [11] | Animation Key Abs Ptr | Absolute pointer to animation key data. The size of the array is always 11. |



#### Animation Key Pointer

| Offset | Type  | Var Name    | Description                         |
| ------ | ----- | ----------- | ----------------------------------- |
| 0x00   |       | Unknown     |                                     |
| 0x04   |       | Unknown     |                                     |
| 0x08   |       | Unknown     |                                     |
| 0x0C   |       | Unknown     |                                     |
| 0x10   | int32 | Key Count   | Can be 0.                           |
| 0x14   | int32 | Key Abs Ptr | Absolute pointer to Animation Keys. |



##### Animation Keys

NOTE: suspiciously similar to [Topology Transform Parameter](#topology-transform-parameter). Look into it further.

This looks to be a Maya 4.X KeyableAttribute Key. The application separates each component of the Transform (position/rotation/scale) as (X, Y, Z) as each are independently keyed. For animations in the scene, light intensity is also a KeyableAttribute it seems.

| Offset | Type  | Name    | Description          |
| ------ | ----- | ------- | -------------------- |
| 0x00   |       | Unknown | Some kind of flag?   |
| 0x04   |       | Time    |                      |
| 0x08   | float | Value   |                      |
| 0x0C   | float | Unknown | Always same as 0x10. |
| 0x10   | float | Unknown | Always same as 0x0C. |



Anim Index:

0 Scale X?

1 Scale Y?

2 Scale Z?

3 Rotation X?

4 Rotation Y?

5 Rotation Z?

6 Position X?

7 Position Y? 

8 Position Z?

9 - "Unused" but not confirmed empty

10 - Light