## Track Unknown Flags 2



## Track Unknown Flags 3

Bits not listed are unused.

| Flags |                                            |      |
| ----- | ------------------------------------------ | ---- |
| Bit 2 | Enabled when float 0x40 is non-zero        |      |
| Bit 3 | Enabled when float 0x3C is non-zero        |      |
| Bit 4 | Has extra data. Probably another use, too. |      |
| Bit 5 | Has extra data. Probably another use, too. |      |



## Track Unknown Option 1

| Options | Pattern                                                      |      |
| ------- | ------------------------------------------------------------ | ---- |
| 0       | Default                                                      |      |
| 1       | Is _sometimes_ selected when (Enum0x00 is bit1 and Enum0x01 and Enum0x02 are both 0) OR when (Enum0x00 is 0 and Enum0x01 is 1 << 7) |      |
| 2       | Is selected when Enum0x00, Enum0x01, and Enum0x02 are all 0  |      |

