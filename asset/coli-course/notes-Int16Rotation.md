Unknown Flags

Index values:

* 8, 9, 10, 11, 12

Index sets (all unique):

* 8 (total: 2)

  * Story 5 on objects `44LASTGATE1`, `44LASTGATE2`
  * THESE OBJECTS HAVE NO MODEL, HAVE (0,0,0) POSITION, ROTATION, (1,1,1) SCALE

* 9, 10, 11 (total: 14)

  * Story 8 on objects `MINE01_MINE`
  * Hypothesis: Normal mission mines

  

* 9, 10, 11, 12 (total: 238)

  * Applies to objects on 23 stages
    * 1, 3, 5, 8, 10, 11, 13, 14, 15, 16, 17, 26, 27, 28, 29, 32, 34, 35, 38, 38, 40, 41, 43
  * Objects names:
    * `AAA`, `BBB`, etc. Note, not all objects of this nature.
    * `MINE10_MINE`
    * `BIL` buildings on stage 5
    * `NOOUT01` - no out - off course?
    * `*KABE*` objects
    * `WALTOP` - wall top - walls?
    * etc
  * Appears to be some kind of collision object tag

* 10, 11

  * Story 8 on objects `MINE01_MINE`
  * Hypothesis: Hard mission mines?

* 11

  * Story 8 on objects `MINE01_MINE`
  * Hypothesis: Very Hard mission mines

