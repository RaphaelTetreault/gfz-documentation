# Track Graph Hierarchy Notes

## Embded Collider Properties

### Stage 01 MCTR

Values are `0.28` in each field (left and right). This seems to correspond to 28% from the outside in. The track width total is 240 units. 28% is 67.2 units. My ruler got very close to that with vertex snapping on the visual mesh (not the collider mesh). 

The only parameter use was index `[0]`, or Scale X.

Topology metadata: `IsUsingTrackProperty`, Track property: `Is Heal Pit`



The track stack `[4]` has no collisions along animation curves.

