Effect Triggers

1. Lightning: no rain; Big Blue: water splash; Outer Space: no meteors
2. Sand Ocean: light sandstorm effect
3. (not used)
4. (not used)
5. Green Plant: leaves;
6. Green Plant: faster, erratic leaves;
7. (not used)
8. Green Plant: other type of leaves...
9. (not used)
10. (not used)
11. (not used)
12. (not used)
13. Outer Space: no ashes
14. Ch 14: dimmer lighting; Sand Ocean: dimmer lighting under path;
15. Big Blue: heavy blue fog (water underpass); Sand Ocean: very dark fog (in pyramid); CH8: less red fog (main chamber / start));
16. (not used)



Animation Triggers

ID = sequence, likely consumed by some scripting code

* ST03 MCSG: enables holographic ads opening up overhead on upper ledge after jump
* 