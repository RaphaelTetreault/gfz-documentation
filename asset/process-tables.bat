:: go to directory to run scripts
CD process/tables/
:: delete temp files
CALL delete-temp-files.bat
:: convert Excel tables to CSV files
CALL xlsx-to-csv.bat
:: convert CSV files to markdown tables
START /WAIT python csv-to-md.py

:: drop down 1 directory
CD ..
:: process all template files, include markdown tables
START /WAIT python process-includes.py

:: drop back to initial ddirectory
CD ..
:: Copy all files from source into destination (files only)
:: /xf <FileName>[...]
::      Excludes files that match the specified names or paths.
::      Note that FileName can include wildcard characters (* and ?).
:: Parameters:
::      param1: source folder
::      param2: destination
:: Options:
::      process.    files to be processed
::      *.bat       BATCH files
::      *.py        Python scripts
::	.gitignore
:: use: robocopy <source> <destination> [<file>[ ...]] [<options>]
ROBOCOPY ./process ./ /xf process.* *.bat *.py .gitignore