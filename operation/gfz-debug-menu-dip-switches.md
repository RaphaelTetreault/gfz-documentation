Translation by: Joe  [Post on Jul forums (TCRF)](http://jul.rustedlogic.net/newreply.php?id=16498&postid=426212)

| DIP  Name       | Menu Description                   | Translation                                 |
| --------------- | ---------------------------------- | ------------------------------------------- |
| DIP_DEBUG       | デバッグを有効にする               | Enable debug                                |
| DIP_DISP        | デバッグ表示を行う                 | Display debug                               |
| DIP_PERF        | パフォーマンスモニタ常に表示       | Constantly display performance  monitor     |
| DIP_NO_PERF     | パフォーマンスモニタ非表示         | Hide performance monitor                    |
| DIP_DISP_COLI   | コリジョンを表示する               | Display collision                           |
| DIP_DISP_LAP    | 順位とタイム表示                   | Display rank and time                       |
| DIP_CARID_DISP  | 車番号表示                         | Display car number                          |
| DIP_EN_LINE     | 敵車走行ラインを取得               | Get the enemy car path                      |
| DIP_LINE_DISP   | 敵車走行ラインを表示               | Display the enemy car path                  |
| DIP_NO_RACE_SEQ | レースのシーケンスを行わない       | Don't do race sequence                      |
| DIP_NO_DEAD     | リタイアしない                     | Don't retire racers                         |
| DIP_NO_DAMAGE   | 車体はダメージを受けない           | Cars don't receive damage                   |
| DIP_AUTODRIVE   | １Ｐ以外の車を自動操縦             | Cars autopilot except P1                    |
| DIP_NO_BG       | バックグラウンド非表示             | Hide background                             |
| DIP_NO_SCREFC   | スクリーンエフェクトＯＦＦ         | Turn screen effects off                     |
| DIP_EFFECT_MAX  | エフェクトＭＡＸ                   | Maximum effects                             |
| DIP_CPU_MAX     | ＣＰＵ　ＭＡＸ                     | Maximum CPU                                 |
| DIP_EMBLEM_USE  | 常にエンブレムの負荷をかける       | Always apply emblem load                    |
| DIP_NO_DISP_IMM | 半とう明のみ表示（Ｄｅｂｕｇのみ） | Only display semi-transparent  (debug only) |
| DIP_NO_EFFECT   | エフェクトＯＦＦ                   | Effects off                                 |
| DIP_FONT_OFF    | ＡＲＡＭフォント非表示             | Hide ARAM font                              |
| DIP_SPR_OFF     | スプライト非表示                   | Hide sprites                                |
| DIP_CAR_LOD     | くるまＬＯＤ                       | Car LOD                                     |
| DIP_CAR_NODISP  | くるま非表示                       | Hide car                                    |
| DIP_CAR_MAXSIZE | くるま最悪の組合せでＬｏａｄ       | Load the worst combination of  cars         |
| DIP_MIPMAP_DISP | ＭＩＰＭＡＰを表示                 | Display mipmap                              |
| DIP_MASK_MIPMAP | ＭＩＰＭＡＰ最上位をマスクする     | Mask the top mipmap                         |
| DIP_PHYS_DEBUG  | 物理デバッグ                       | Physics debug                               |
| DIP_PHYS_NODISP | 物理オブジェ非表示                 | Hide physics objects                        |
| DIP_SEL_DEVELOP | 開発中のセレクタに行きます         | Go to the development selector              |
| DIP_CAM_CTRL    | ＣＡＭコントロール　ｆｒｏｍ　２Ｐ | Control camera from P2                      |
| DIP_FOR_PUB     | パブ用ＤＩＰ（　用）               | DIP for pub (for )                          |