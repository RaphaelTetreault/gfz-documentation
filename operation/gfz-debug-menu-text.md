Translations of the debug menu text.

circle = Will Freeze

dash = May Freeze in certain conditions
x = Softblocks
equals_sign = Null or Void

| Debug  Menu                                      | Translation                                           | Description                                                  | Freezes? |
| ------------------------------------------------ | ----------------------------------------------------- | ------------------------------------------------------------ | -------- |
| ディップスイッチ                                 | DIP Switch                                            | [Select DIP options. Uncertain if it   has any effect. Refer to list.](https://docs.google.com/spreadsheet/ccc?key=0AiANfINZ0c74dG0xOEkxYU54MkdfRWRhdWk5cjNBOFE#gid=0) |          |
| パッド入力                                       | PAD Input                                             | View control input, to test both controller and steering wheel  input. *Note: Exit with Z+Start. |          |
| モーション再生                                   | Motion Playback                                       | Character debugger, used to test out character models of  different resolutions and character animations. |          |
| ビットマップ                                     | Bitmap                                                | Graphic viewer and debugger, used to view any graphic from any  archive within the bitmap directory. |          |
| ARAM  フォント                                   | ARAM Font                                             | View different fonts. *Warning:  will crash when you view FONT_ASC_24x24. | -        |
| ステージチェック                                 | Stage Check                                           | [Used to roam around stages. Will   freeze upon loading a non-existant stage. Refer to list.](https://docs.google.com/spreadsheet/ccc?key=0AiANfINZ0c74dF9KTnM3VHh1aU9Wc09DN1hVaWs3ZlE&usp=drive_web) | -        |
| くるまチェック  C カラー, Y しょぼ, X ふつうしゃ | Car Check C Color, Y Manual  Book, X Default Car Size | Freezes.                                                     | ●        |
| モデルテスト                                     | Model Test                                            | 3D model viewer, used to view  anything from Characters to Vehicles to Stages. (Use L/R) |          |
| サウンドテスト                                   | Sound Test                                            | Play sound effects, character  dialogue and the game's soundtrack. |          |
| ぶつりテスト                                     | Physics Test                                          | Unsure how to get anything to  work in this menu.            | =        |
| ぬのテスト                                       | Cloth Test                                            | Nullified menu option. Does  nothing.                        | =        |
| パーツくるまチェック                             | Vehicle Parts Check                                   | Garage debugger, used to test  out different machine parts, colours and pilots. |          |
| ネームエントリー                                 | Name Entry                                            | Rewrites player's name.                                      |          |
| テスト走行                                       | Test Run                                              | Play a custom machine on Sonic  Oval, Twist Road or Thunder Road. |          |



| Input                | Controls                                                     |
| -------------------- | ------------------------------------------------------------ |
| Start                | Go Back (not menus 1,  2, and 3.)                            |
| A                    | Enter, Play or Load.                                         |
| B                    | Go Back for menu 1 and  3, hide text in menu 4. Exit main menu. |
| X                    | Not used?                                                    |
| Y                    | Lock on item / zoom in  on item (very useful.)               |
| Z                    | Turn Near mode on or  off.                                   |
| L                    | "Freeze"  view (useful when looking at model textures.)      |
| R                    | Hold R + Analogue Up  or Down to rotate around object vertically (Around center axis, Y/Z plane).  Hold R + C-Stick Up or Down to rotate Player's View (Rotate around self). |
| Analogue  Left/Right | Rotate Left/Right  around center axis (X/Z plane, around self.) |
| Analogue  Up/Down    | Select option in main  menu, zoom in/out of center point.    |
| C-Stick  Left/Right  | Rotate your viewpoint  Left or Right (Rotate around self.)   |
| C-Stick  Up/Down     | Holding R, rotate your  viewpoint up or down (Rotate around self.) |
| D-Pad                | Used to select files  within menus. Up/down to select container/option, left/right to select files  within container or change option. |